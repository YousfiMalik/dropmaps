﻿using System;
using CoreLocation;
using Foundation;
using GMCluster;
using ObjCRuntime;
using UIKit;

namespace ClusteringMapXamarinForms
{
	public class DropInMap : UIView,IGMUClusterItem
	{ 
		public int id; 
		public string type;
		public float latitude; 
		public float longitude; 
		public float altitude; 
		public string thumbnail; 
		public string emoji; 
		public DateTime registered; 
		public int follow;
		public int eventspecific; 
		public int venuespecific;
		public DropInMap()
		{
			
		}

		public DropInMap(int id, string type, float latitude, float longitude, string thumbnail, string emoji, DateTime registered)
		{
			this.id = id;
			this.type = type;
			this.Position = new CLLocationCoordinate2D(latitude, longitude);
			this.latitude = latitude;
			this.latitude = latitude;
			this.thumbnail = thumbnail;
			this.emoji = emoji;
			this.registered = registered;
		}

		public DropInMap(double latitude, double longitude, string name)
		{
			Position = new CLLocationCoordinate2D(latitude, longitude);
			Name = name;
		}

		public DropInMap(CLLocationCoordinate2D position, string name)
		{
			Position = position;
			Name = name;
		}


		public CLLocationCoordinate2D Position
		{
			get;
			private set;
		}

		public string Name
		{
			get;
			private set;
		}


	}
}

