﻿using System;
using CoreGraphics;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

namespace ClusteringMapXamarinForms.iOS
{
	public partial class MyDropView : UIViewController
	{
		public MyDropView() : base("MyDropView", null)
		{
		}

		public override void LoadView()
		{
			base.LoadView();
		}

		public MyDropView(int id, string type, string thumbnail, string emoji) : base("MyDropView", null)
		{
			base.LoadView();

	//	this.View.Frame = new CGRect(0, 0, UIScreen.MainScreen.Bounds.Width-650f, UIScreen.MainScreen.Bounds.Height-800f);
			this.View.BackgroundColor = Color.Transparent.ToUIColor();
			this.thumbnail.Image = UIImage.FromBundle(thumbnail);
			//this.thumbnail.Layer.CornerRadius = -12;
			this.thumbnail.BackgroundColor = Color.Transparent.ToUIColor();
			this.emoji.BackgroundColor = Color.Transparent.ToUIColor();
			this.emoji.Image = UIImage.FromBundle(emoji);
			//	UIView myView = new UIView();
			if (type.Equals("Challenge"))
			{
				//this.View.BackgroundColor = Color.Transparent.ToUIColor();
				this.description.BackgroundColor = Color.FromRgb(42, 44, 51).ToUIColor();
				this.description.TextColor = Color.White.ToUIColor();
				this.description.Text = "SICK TRICK CONTEST";
			}
			else
			{
				this.description.RemoveFromSuperview();
			}
		}

		public UIImageView getThumbnail()
		{
			//this.thumbnail.BackgroundColor = Color.Black.ToUIColor();
			return this.thumbnail;
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			// Perform any additional setup after loading the view, typically from a nib.


			//	this.thumbnail.Image = UIImage.FromBundle("pic2");
			//	this.thumbnail.Frame = new CGRect(150, 150, 100, 100);

			//this.emoji.Image= UIImage.FromBundle("pic1");;
			//	this.emoji.Frame = new CGRect(200,200 , 40, 40);
			//	this.description.Text = "hi i'm here";

			//	this.description.Frame = new CGRect(150, 150, 100, 100);

			//var user = new UIViewController();
			//user.View.BackgroundColor = UIColor.Magenta;

			/*	btn.TouchUpInside += (sender, e) =>
				{
					this.NavigationController.PushViewController(user, true);
				};*/
		}

		public UIView getView()
		{
			return this.View;
		}

		public override void DidReceiveMemoryWarning()
		{
			base.DidReceiveMemoryWarning();
			// Release any cached data, images, etc that aren't in use.
		}
	}
}

