﻿using System;
using Xamarin.Forms.GoogleMaps;

namespace ClusteringMapXamarinForms
{
	public class CustomPin : Map
	{
        
        
		public Pin Pin { get; set; }
		public string Id { get; set; }
		public string Url { get; set; }
		public CustomPin()
		{
		}
		public CustomPin(Pin Pin, String Id, String Url)
		{
			this.Pin = Pin;
			this.Id = Id;
			this.Url = Url;
		}
	}

	}
