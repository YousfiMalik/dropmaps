﻿using System;
using Xamarin.Forms.Maps;

namespace ClusteringMapXamarinForms
{
	public class CustomPinForAndroid
	{
		public CustomPinForAndroid()
		{
		}
		public Pin Pin { get; set; }
		public string Id { get; set; }
		public string Url { get; set; }
		public CustomPinForAndroid(Pin Pin, String Id, String Url)
		{
			this.Pin = Pin;
			this.Id = Id;
			this.Url = Url;
		}
	}
}
