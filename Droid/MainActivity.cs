﻿using System;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;

namespace ClusteringMapXamarinForms.Droid
{
	[Activity(Label = "ClusteringMapXamarinForms.Droid", Icon = "@drawable/icon", Theme = "@style/MyTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
	public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
	{
		protected override void OnCreate(Bundle savedInstanceState)
		{

			base.OnCreate(savedInstanceState);
			TabLayoutResource = Resource.Layout.Tabbar;
			ToolbarResource = Resource.Layout.Toolbar;
			global::Xamarin.Forms.Forms.Init(this, savedInstanceState);
			App.ScreenHeigh = (int)((float)Resources.DisplayMetrics.HeightPixels / (float)Resources.DisplayMetrics.Density);
			App.ScreenWidh = (int)((float)Resources.DisplayMetrics.WidthPixels / (float)Resources.DisplayMetrics.Density);
			LoadApplication(new App());
		}
	}
}
